/*********************************************************************
* The sampleMetricUploadEndpoint file defines a custom endpoint interface
* that will be reachable through an HTTP POST request to
* <velocity-url>/reporting-consumer/pluginEndpoint/<integration-id>/sampleMetricUploadPath.
* The fully resolved endpoint path will be displayed in the details pane of the
* integration after it is created in the UrbanCode Velocity Integrations page.
*
* Note that the handleEndpoint function will only be executed when the
* endpoint is hit via an HTTP POST request. Both the endpoint path
* and method are defined in the exported object.
***********************************************************************/
import log4js from '@velocity/logger'
import UCVClient from '../api/ucvClient'
import printSampleProperties from '../samples/samplePropertyPrinter'
import SampleDataGenerator from '../samples/sampleDataGenerator'
import { METRIC_DEFINITION_ID } from '../constants/sampleConstants'

/* Acquire logger with redacted secure values. */
const LOGGER = log4js.getLogger('SampleEndpoint')

/* The main function called when an HTTP request is made against this endpoint. */
async function handleEndpoint (body, opts) {
  LOGGER.info('This is a sample endpoint implemented in a UCV Extension Package!')

  /*
    When executing endpoints, properties defined in the index.js are
    accessed via the opts.integration.properties object.
  */
  const uploadSampleData = opts.integration.properties.uploadSampleData

  /* Properties of the integration itself are accessed through props.integration. */
  const tenantId = opts.integration.tenant_id
  const integrationId = opts.integration._id

  /*
    Parameters or Properties of the HTTP request are accessed through the
    body object. Since this endpoint expects an HTTP POST request, the
    body will be resolved from the request body of the HTTP call.

    You can access individual properties via body.<property-name>.
  */
  LOGGER.info(`The request body:\n
    ${JSON.stringify(body)}`
  )

  /* Display all of the sample properties for demonstrative purposes. */
  printSampleProperties(opts.integration.properties)

  /*
    The 'Upload Sample Data' boolean property will upload a sample Metric into
    UrbanCode Velocity when checked.
  */
  if (uploadSampleData) {
    LOGGER.info('Sample metric data will be uploaded to UrbanCode Velocity.')

    const ucvAccessKey = opts.integration.properties.ucvAccessKey
    UCVClient.initialize(process.env.GRAPHQL_URL, ucvAccessKey)
    SampleDataGenerator.initialize(integrationId, tenantId)

    /* The metric definition must be defined before it can be used. */
    if (!(await UCVClient.doesMetricDefinitionExist(METRIC_DEFINITION_ID, tenantId))) {
      await UCVClient.createMetricDefinition(METRIC_DEFINITION_ID, tenantId)
    }

    /* Upload sample metric data. */
    const sampleMetrics = SampleDataGenerator.getSampleMetricData()
    await UCVClient.uploadMetrics(sampleMetrics)
    LOGGER.info('Sample metric data uploaded successfully.')
  }
}

export default {
  execute: handleEndpoint,
  name: 'Sample Metric Upload Endpoint',
  path: 'sampleMetricUploadPath',
  method: 'POST'
}

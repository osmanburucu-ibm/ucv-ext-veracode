/*********************************************************************
* The SecondSampleEndpoint file defines a custom endpoint interface
* that will be reachable through an HTTP GET request to
* <velocity-url>/reporting-consumer/pluginEndpoint/<integration-id>/secondSampleEndpointPath.
* The fully resolved endpoint path will be displayed in the details pane of the
* integration after it is created in the UrbanCode Velocity Integrations page.
*
* Note that the handleEndpoint function will only be executed when the
* endpoint is hit via an HTTP GET request.
*
* This endpoint does nothing except demonstrate the ability for a single
* plugin to define multiple endpoints. Both the endpoint path
* and method are defined in the exported object.
***********************************************************************/
import log4js from '@velocity/logger'

/* Acquire logger with redacted secure values. */
const LOGGER = log4js.getLogger('SecondSampleEndpoint')

function handleEndpoint (body, opts) {
  LOGGER.info('This is another sample endpoint implemented in a UCV Extension Package!')
}

export default {
  execute: handleEndpoint,
  name: 'Second Sample Endpoint',
  path: 'secondSampleEndpointPath',
  method: 'GET'
}

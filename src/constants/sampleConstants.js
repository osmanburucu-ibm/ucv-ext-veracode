/*********************************************************************
* The sampleConstants file exports some constant fields that will
* be reused throughout the plugin. These fields are used only for demonstration
* purposes.
*
* APPLICATION_NAME would normally come an application that exists on an
* external integration server.
*
* METRIC_DATA_SET and METRIC_DEFINITION_ID are used to categorize metrics
* in the insights view of UrbanCode Velocity.
*
* METRIC_DATA_SET defines a set of metric records, and each record in the
* data set can have a separate METRIC_DEFINITION_ID if necessary.
***********************************************************************/
export const APPLICATION_NAME = 'JPetStore'
export const METRIC_DATA_SET = 'StaticScan'
export const METRIC_DEFINITION_ID = 'Security Static Scan'

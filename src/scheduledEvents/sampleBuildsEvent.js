/*********************************************************************
* The sampleBuildsEvent file defines a scheduled event that
* will upload some sample builds into UrbanCode Velocity.
*
* Scheduled events execute on a timed interval defined in the scheduled
* event object (see the export statement at the end of this file).
***********************************************************************/
import log4js from '@velocity/logger'
import UCVClient from '../api/ucvClient'
import printSampleProperties from '../samples/samplePropertyPrinter'
// import printSampleValues from '../samples/sampleValuePrinter'
import SampleDataGenerator from '../samples/sampleDataGenerator'

const appdata = require('../vc/vc-appdata')

/* Acquire logger with redacted secure values. */
const LOGGER = log4js.getLogger('SampleBuildsUploadEvent')

async function execute (state, properties) {
  /*
    When executing scheduled events, properties defined in the index.js are
    accessed directly via properties object passed to the execute function.
  */
  /* const uploadSampleData = properties.uploadSampleData */
  const uploadSampleData = true

  /*
    Integrations have a state object that gets passed to scheduled events through
    the execute function.

    The state tracks the last time each event ran, it also provides the scheduled
    event access to properties of the integration itself.
  */
  const integrationId = state.trackerId // The trackerId is the ID of the integration
  const tenantId = state.tenantId

  /*
    The lastRun field is used to acquire data from an integration server
    after a given time. Every time the integration completes execution it
    updates the lastRun field on the state with the exact Unix timestamp of
    when it completed (number of milliseconds since Unix epoch).

    The exact syntax for using this field depends on the external integration
    server API that you're integrating with. For instance, the Azure DevOps
    REST API utilizes the System.ChangedDate field when querying for work items.

    To retrieve work items modified after the last execution of the plugin
    we could make the following query with the Azure DevOps server:
    Select * From WorkItems Where System.ChangedDate > `${new Date(lastRun).toUTCString()}
  */
  const lastRun = state.lastRun
  properties.lastRun = lastRun

  LOGGER.info(`This integration was last executed on ${new Date(lastRun).toUTCString()}`)

  /* Display all of the sample properties for demonstrative purposes. */
  printSampleProperties(properties)

  if (uploadSampleData) {
    LOGGER.info('Sample build data will be uploaded to UrbanCode Velocity.')

    const ucvAccessKey = properties.ucvAccessKey
    UCVClient.initialize(process.env.GRAPHQL_URL, ucvAccessKey)
    SampleDataGenerator.initialize(integrationId, tenantId)

    properties.integrationId = integrationId
    properties.tenantId = tenantId
    properties.UCVClient = UCVClient

    /* Upload sample build data. */
    /* use this for getting data from veracode */
    // const sampleBuild = SampleDataGenerator.getSampleBuildData()
    const sampleMetrics = SampleDataGenerator.getSampleMetricData(properties)
    properties.sampleMetrics = sampleMetrics
    appdata.getAppID(properties)
    // printSampleValues(sampleMetrics)

    /* not use uploadBuild use */
    /*     await UCVClient.uploadBuild(sampleBuild) */
    // await UCVClient.uploadMetrics(sampleMetrics)
    // Save the system time to the integration
    /*     await UCVClient.uploadProperties(integrationId, 'SampleBuildUploadEvent', { key: 'value' })  */

    // LOGGER.info('Sample build data uploaded successfully.')
  }
}

export default {
  execute: execute,
  name: 'SampleBuildUploadEvent',
  description: 'This is a timed event that uploads sample builds.',
  interval: 5
}

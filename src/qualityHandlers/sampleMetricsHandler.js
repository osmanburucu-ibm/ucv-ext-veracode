/*********************************************************************
* The sampleMetricsHandler file defines a quality handler (parser) that
* will upload some sample metrics. Quality handler interfaces (parsers)
* do not rely on the creation of an integration in UrbanCode Velocity.
*
* The quality handler (parser)'s execute function will be run when an
* HTTP POST request is sent to the <velocity-url>/reporting-consumer/metrics
* endpoint.
*
* Quality handlers (parsers) are similar to endpoints (see the endpoints folder)
* because they are both initialized only after an HTTP request is made. However,
* the quality handlers (parsers) are passed a test artifact file that is parsed
* and the data in the file is normalized to create UrbanCode Velocity metrics.
***********************************************************************/
import { METRIC_DATA_SET, METRIC_DEFINITION_ID } from '../constants/sampleConstants'
import log4js from '@velocity/logger'
import fs from 'fs'

/* Acquire logger with redacted secure values. */
const LOGGER = log4js.getLogger('sampleMetricsHandler')

async function handleQualityData (payload, opts) {
  LOGGER.info('Executing the sample metrics handler endpoint.')

  /*
    When a parser plugin is executed, two separate HTTP forms are expected on
    the HTTP POST request. The first form contains a form-field called 'payload'
    and the second form contains a form-field called 'testArtifact'.

    An example curl request:
    curl --request POST \
    --url <velocity-url>/reporting-consumer/metrics \
    --form 'payload={...}' \
    --form 'testArtifact=@filename

    The first form (the payload) is passed as the first argument (payload) to
    the handleQualityData endpoint, and the second form (the testArtifact)
    consists of the file containing metric data and it is passed as the second
    argument (opts).

    The following fields are required on the payload.
  */
  const tenantId = payload.tenantId
  const application = payload.application.name
  const dataFormat = payload.record.dataFormat

  /*
    Aside from the plugin configuration, the pluginType is also passed in
    the payload object. This field tells UrbanCode Velocity which quality handler
    will be used to parse the data sent.

    Note that this pluginType needs to match the type field exported in the
    quality handler (for example, see the export statement at the end of this
    file).

    This print out is only executed for demonstrative purposes.
  */
  const pluginType = payload.record.pluginType
  LOGGER.info(`Parsing test artifact with quality handler type: ${pluginType}`)

  const metrics = []

  /* Acquire JSON contents from the testArtifact */
  const testPath = opts.testArtifact.path
  const testFile = fs.readFileSync(testPath)
  const testJson = JSON.parse(testFile)

  /* Extract and normalize metric data from the testArtifact JSON */
  if (testJson && Array.isArray(testJson)) {
    testJson.forEach(testObject => {
      metrics.push({
        tenantId: tenantId,
        dataSet: METRIC_DATA_SET,
        application: application,
        record: {
          metricDefinitionId: METRIC_DEFINITION_ID,
          recordName: testObject.name,
          dataFormat: dataFormat,
          executionDate: testObject.executionDate,
          valueType: 'countset',
          value: {
            pass: testObject.pass,
            fail: testObject.fail
          },
          entries: testObject.entries
        }
      })
    })
  }

  /*
    The return value from the handleQualityData plugin must be an array
    that contains all of the normalized metrics from the testArtifact.
    UrbanCode Velocity will sync this data when it is returned from the plugin.
  */
  return metrics
}

export default {
  execute: handleQualityData,
  name: 'Sample Quality Quality Data',
  description: 'This handles a simple json doc that will be handed to the Application API',
  type: 'samplePluginHandler'
}

/*********************************************************************
* The index.js defines the configurable properties and interfaces of a
* plugin. The available plugin interface types are endpoints, sheduled events,
* and qualityHandlers (AKA parsers).
*
* The exported plugin interface objects contain an execute function. When
* Velocity executes those interfaces, it runs the execute function provided.
* For some examples check out the javascript files in the scheduledEvents,
* endpoints, and qualityHandlers folders.
***********************************************************************/
/*
import sampleMetricsEndpoint from './endpoints/sampleMetricsEndpoint'
import secondSampleEndpoint from './endpoints/secondSampleEndpoint'
*/

import sampleBuildUploadEvent from './scheduledEvents/sampleBuildsEvent'

/*
import sampleIssueUploadEvent from './scheduledEvents/sampleIssuesEvent'
import sampleMetricsHandler from './qualityHandlers/sampleMetricsHandler'
*/

/*
  Note that if you do not want to execute the sample interfaces, you can
  remove them from the exported endpoints, scheduledEvents, and qualityHandlers
  here.
*/
export default {
  properties: [
    // {
    //   name: 'username',
    //   label: 'Username',
    //   type: 'String',
    //   description: 'Name of your Veracode user',
    //   required: true
    // },
    // {
    //   name: 'password',
    //   label: 'Password',
    //   type: 'Secure',
    //   description: 'Password for your VeraCode User',
    //   required: true
    // },
    {
      name: 'api_id',
      label: 'API ID',
      type: 'Secure',
      description: 'API ID for your VeraCode User',
      required: true
    },
    {
      name: 'api_key',
      label: 'API KEY',
      type: 'Secure',
      description: 'API KEY for your VeraCode User',
      required: true
    },
    {
      name: 'nameOfApp',
      label: 'VeraCode Application Name',
      type: 'String',
      description: 'Name of Application which is scanned in VeraCode',
      required: true
    },
    {
      name: 'nameOfPipelineApp',
      label: 'Pipeline Application Name',
      type: 'String',
      description: 'Name of Pipeline Application',
      required: true
    },
    {
      name: 'ucvAccessKey',
      label: 'UrbanCode Velocity User Access Key',
      type: 'Secure',
      description: 'The user access key to authenticate with the UrbanCode Velocity server.',
      required: false
    }
  ],
  /*  endpoints: [sampleMetricsEndpoint, secondSampleEndpoint],  */
  /*   scheduledEvents: [sampleIssueUploadEvent, sampleBuildUploadEvent], */
  scheduledEvents: [sampleBuildUploadEvent],

  /*  taskDefinitions: [], // Currently not supported
  eventTriggers: [], // Currently not supported
  qualityHandlers: [sampleMetricsHandler],
*/

  /*
    A pipelineDefinition can be used to configure your plugin in a pipeline.
    For more information view the 'Pipeline Definition' section of the README.
    The following provides an example:
    pipelineDefinition: {
      importsApplications: false,
      importsVersions: false,
      importsEnvironments: false
    },
  */
  displayName: 'VeraCode Plugin', // How the plugin shows in the UI
  pluginId: 'ucv-ext-veracode', // Must match the 'name' in package.json
  description: 'This is a plugin to integrate VeraCode application security scanning with UrbanCode Velocity.'
}

/*********************************************************************
* This UCVClient class utilizes the api-client module to execute queries and mutations
* defined in UrbanCode Velocity's GraphQL API.
* Some sample API calls are defined here, and you can find the full GraphQL API
* documentation for your server at <velocity-url>/release-events-api/graphiql
***********************************************************************/
import { VelocityApi } from '@velocity/api-client'
import log4js from '@velocity/logger'

const LOGGER = log4js.getLogger('UCVClient')

export default class UCVClient {
  static async initialize (serverUrl, securityToken) {
    if (!this.binding) {
      LOGGER.info('Initializing Velocity API Client for the template integration.')
      try {
        this.binding = new VelocityApi(serverUrl, securityToken, { insecure: true, useBearerToken: false })
      } catch (error) {
        LOGGER.error(error)
        throw error
      }
    }
  }

  /*
    Upload issues into UrbanCode Velocity. Please see the uploadIssueData mutation
    in the full GraphQL API documentation of your server for more information.
  */
  static async uploadIssues (issueDataIn) {
    LOGGER.debug(`Issue sync data: ${JSON.stringify(issueDataIn)}`)
    try {
      const result = await this.binding.mutation.uploadIssueData({ data: issueDataIn })
      return result
    } catch (error) {
      LOGGER.error(error)
      throw error
    }
  }

  /*
    Upload pull requests into UrbanCode Velocity. Please see the uploadPullRequests mutation
    in the full GraphQL API documentation of your server for more information.
  */
  static async uploadPullRequests (pullRequestDataIn) {
    LOGGER.debug(`Pull Request sync data: ${JSON.stringify(pullRequestDataIn)}`)

    try {
      const result = await this.binding.mutation.uploadPullRequests({ data: pullRequestDataIn })
      return result
    } catch (error) {
      LOGGER.error(error)
      throw error
    }
  }

  /*
    Upload commits into UrbanCode Velocity. Please see the uploadCommits mutation
    in the full GraphQL API documentation of your server for more information.
  */
  static async uploadCommits (commitDataIn) {
    LOGGER.debug(`Commit sync data: ${JSON.stringify(commitDataIn)}`)

    try {
      const result = await this.binding.mutation.uploadCommits({ data: commitDataIn })
      return result
    } catch (error) {
      LOGGER.error(error)
      throw error
    }
  }

  /*
    Upload a build into UrbanCode Velocity. Please see the uploadBuildData mutation
    in the full GraphQL API documentation of your server for more information.
  */
  static async uploadBuild (buildDataIn) {
    LOGGER.debug(`Build sync data: ${JSON.stringify(buildDataIn)}`)

    try {
      const result = await this.binding.mutation.uploadBuildData({ data: buildDataIn })
      return result
    } catch (error) {
      LOGGER.error(error)
      throw error
    }
  }

  /* Create a metric definition. A metric definition is necessary for uploading metrics.
    Please see the createMetricDefinition mutation in the full GraphQL API documentation
    of your server for more information. */
  static async createMetricDefinition (metricDefinitionId, tenantId) {
    try {
      const metricDefinition = {
        id: metricDefinitionId,
        name: metricDefinitionId,
        category: 'quality',
        tenantId: tenantId
      }
      LOGGER.debug(`Creating metric definition with the following input: ${JSON.stringify(metricDefinition)}.`)
      await this.binding.mutation.createMetricDefinition({ input: metricDefinition })
    } catch (error) {
      LOGGER.error(error)
      throw error
    }
  }

  /*
    Check if a metric definition exists. This is useful to ensure you don't
    try to create an existing metric definition. Please see the metricDefinition query
    in the full GraphQL API documentation of your server for more information.
  */
  static async doesMetricDefinitionExist (metricDefinitionId, tenantId) {
    const queryArgs = {
      id: metricDefinitionId,
      tenantId: tenantId
    }

    LOGGER.debug(`Running metricDefinition query with query arguments ${JSON.stringify(queryArgs)}`)
    try {
      const metricDefinition = await this.binding.query.metricDefinition({ query: queryArgs })
      LOGGER.debug(`Metric definition found: ${JSON.stringify(metricDefinition)}`)
    } catch (error) {
      LOGGER.debug('No metric definition found.')
      return false
    }

    return true
  }

  /*
    Upload metrics into UrbanCode Velocity. Please see the metricDefinition query
    in the full GraphQL API documentation of your server for more information.
  */
  static async uploadMetrics (metricDataIn) {
    LOGGER.debug(`Metric sync data: ${JSON.stringify(metricDataIn)}.`)
    try {
      await this.binding.mutation.uploadMetrics({ data: metricDataIn })
    } catch (error) {
      LOGGER.error(error)
      throw error
    }
  }

  /*
    Upload some extra properties that will be available in the 'state' param for scheduledEvents
  */
  static async uploadProperties (integrationId, eventName, properties) {
    try {
      const result = await this.binding.mutation.updateIntegrationState({ integrationId, eventName, properties })
      return result
    } catch (error) {
      LOGGER.error(error)
      throw error
    }
  }
}

import log4js from '@velocity/logger'
import printSampleValues from '../samples/sampleValuePrinter'

/* Acquire logger with redacted secure values. */
const LOGGER = log4js.getLogger('vc-report')

const https = require('https')
const auth = require('./vc-auth')

var options2 = {
  host: auth.getHost(),
  path: '/appsec/v2/applications',
  method: 'GET'
}

var getSummaryReport = (properties) => {
  LOGGER.info('getSummaryReport')
  options2.path = options2.path + '/' + properties.app_guid + '/' + 'summary_report'
  properties.options2 = options2
  makeCall2(properties, function (results) {
    properties.results = results
    handleResults2(properties)
  })

  return 0
}

function makeCall2 (properties, callback) {
  LOGGER.info('makeCall2')
  properties.options2.headers = {
    'Authorization': auth.generateHeader(properties, properties.options2.path, properties.options2.method)
  }
  LOGGER.info('options2.authorization=' + properties.options2.headers['Authorization'])
  LOGGER.info('options2.path=' + properties.options2.path)

  var req2 = https.request(properties.options2, function (res2) {
    var body = ''

    res2.on('data', (chunk) => {
      LOGGER.info('on data')
      body += chunk
    })

    res2.on('end', function () {
      LOGGER.info('on end')
      callback(body)
    })
    res2.on('error', function (e) {
      LOGGER.info('on error')
      LOGGER.error(e)
    })
  })
  req2.on('error', (err) => {
    LOGGER.error(err)
  })

  req2.end()
}

function handleResults2 (properties) {
  LOGGER.info('handleResults2')
  var summaryReport = JSON.parse(properties.results)

  // summaryReport.last_update_time
  // summaryReport.flaw-status.sev-5-change HIGH

  LOGGER.info('last_update_time: ')
  LOGGER.info(summaryReport.last_update_time)

  properties.sampleMetrics.record.value['Very-High'] = summaryReport['flaw-status.sev-5-change']
  properties.sampleMetrics.record.value['High'] = summaryReport['flaw-status.sev-4-change']
  properties.sampleMetrics.record.value['Medium'] = summaryReport['flaw-status.sev-3-change']
  properties.sampleMetrics.record.value['Low'] = summaryReport['flaw-status.sev-2-change']
  properties.sampleMetrics.record.value['Info'] = summaryReport['flaw-status.sev-1-change']

  printSampleValues(properties.sampleMetrics)
  properties.UCVClient.uploadMetrics(properties.sampleMetrics)
}

export {
  getSummaryReport
}

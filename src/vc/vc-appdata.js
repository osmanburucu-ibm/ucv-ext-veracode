import log4js from '@velocity/logger'
/* Acquire logger with redacted secure values. */
const LOGGER = log4js.getLogger('vc-appdata')

const https = require('https')
const auth = require('./vc-auth')
const summaryreport = require('./vc-report')

var options = {
  host: auth.getHost(),
  path: '/appsec/v1/applications?name=',
  method: 'GET'
}

var getApplications = () => {
  LOGGER.info('getApplications')
  var body = ''
  //    body = req.end();
  return body
}

var getAppID = (properties) => {
  LOGGER.info('getAppID')
  options.path = options.path + properties.nameOfApp
  properties.options = options
  makeCall(properties, function (results) {
    properties.results = results
    handleResults(properties)
  })

  return 0
}

function makeCall (properties, callback) {
  LOGGER.info('makeCall')
  properties.options.headers = {
    'Authorization': auth.generateHeader(properties, properties.options.path, properties.options.method)
  }
  LOGGER.info('options.authorization=' + properties.options.headers['Authorization'])
  LOGGER.info('options.path=' + properties.options.path)
  var req = https.request(properties.options, function (res) {
    var body = ''

    res.on('data', (chunk) => {
      LOGGER.info('on data')
      body += chunk
    })

    res.on('end', function () {
      LOGGER.info('on end')
      callback(body)
    })
    res.on('error', function (e) {
      LOGGER.info('on error')
      LOGGER.error(e)
    })
  })
  req.on('error', (err) => {
    LOGGER.error(err)
  })

  req.end()
}

function handleResults (properties) {
  LOGGER.info('handleResults')
  var allApps = JSON.parse(properties.results)

  // name of app is in : object►_embedded►applications►[]►profile►name
  // guid is in: bject►_embedded►applications►[]►guid
  allApps._embedded.applications.forEach((item) => {
    LOGGER.info('ID: ' + item.guid)
    LOGGER.info('Name:' + item.profile.name)
    if (item.profile.name === properties.nameOfApp) {
      LOGGER.info('found it')
      properties.app_guid = item.guid
    }
  })
  LOGGER.info('guid of app is: ')
  LOGGER.info(properties.app_guid)

  summaryreport.getSummaryReport(properties)
}

export {
  getApplications,
  makeCall,
  getAppID
}

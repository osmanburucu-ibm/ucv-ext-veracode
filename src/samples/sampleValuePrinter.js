/*********************************************************************
* The samplePropertyPrinter script provides some functions for displaying
* the sample integration property values (Sample String Property, Sample
* Secure String Property, Sample Boolean Property, Sample Select Property,
* and Sample Dropdown Property).
*
* These sample properties are used only to gain understanding of the syntax
* for defining various properties in UrbanCode Velocity, how they are
* displayed in the UI, and how their values are resolved.
***********************************************************************/
import log4js from '@velocity/logger'
import Table from 'cli-table'

/* Acquire logger with redacted secure values. */
const LOGGER = log4js.getLogger('SampleValuePrinter')

export default function printSampleValues (sampleMetrics) {
  const table = new Table({
    head: ['Property', 'Value'],
    colWidths: [28, 50]
  })
  table.options.chars.left = ' '
  table.options.chars.right = ' '
  table.options.chars.middle = ' '
  table.push(
    ['timestamp', sampleMetrics.record.executionDate.toString() ? sampleMetrics.record.executionDate.toString(): ''],    
    ['High', sampleMetrics.record.value.High.toString() ? sampleMetrics.record.value.High.toString() : ''],
    ['Medium', sampleMetrics.record.value.Medium.toString() ? sampleMetrics.record.value.Medium.toString() : ''],
    ['Low', sampleMetrics.record.value.Low.toString() ? sampleMetrics.record.value.Low.toString() : ''],
    ['Info', sampleMetrics.record.value.Info.toString() ? sampleMetrics.record.value.Info.toString() : '']
  )

  LOGGER.info(`Sample Input Properties:\n${table.toString()}`
  )
}

function getMaxPropLength (properties) {
  const values = Object.values(properties)
  return values.reduce((val1, val2) => {
    const length1 = val1.toString().length
    const length2 = val2.toString().length
    return length1 > length2 ? val1.toString() : val2.toString()
  }).length
}

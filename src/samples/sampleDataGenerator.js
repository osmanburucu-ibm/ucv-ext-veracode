/*********************************************************************
* The SampleDataGenerator class provides some sample data to upload into
* UrbanCode Velocity.
*
* Along with the UrbanCode Velocity GraphQL API documentation, located at
* <velocity-url>/release-events-api/graphiql, the data that this class provides
* can give you an idea of the format of input data for various mutations.
*
* This class is used for sample data only, and should be used solely for
* demonstration.
***********************************************************************/
import { APPLICATION_NAME, METRIC_DATA_SET, METRIC_DEFINITION_ID } from '../constants/sampleConstants'

/* Acquire logger with redacted secure values. */
// import log4js from '@velocity/logger'
// const LOGGER = log4js.getLogger('SampleDataGenerator')

/*
  Issues, builds, commits, pull requests, and metrics are unique in UrbanCode
  Velocity based on their ID/name and their creation date. If the ID/name and
  creation date of an incoming object has already been synced, the same object
  will not be imported again. Additionally, the unique object will only be
  resynced if its fields have changed.

  Since we don't want to continually sync the same sample objects over and over
  every time the sample plugin executes, we are using a static date to determine
  when all of these sample objects were created.
*/
const CREATION_DATE = 1589091055363

export default class SampleDataGenerator {
  static async initialize (integrationId, tenantId) {
    if (!this.integrationId && !this.tenantId) {
      this.integrationId = integrationId
      this.tenantId = tenantId
    }
  }

  /*
    Provides data to demonstrate the format of the MetricsIn object that the
    uploadMetrics mutation is expecting as input (Please see the GraphQL API
    documentation mentioned in the initial comment of this file).

    Note that the data defined in the record field should come from an external
    integration server.
  */
  static getSampleMetricData (properties) {
    // TODO: do the processing here... at the moment doesn't work because of async...
    // appdata.getAppID(properties)
    return {
      tenantId: this.tenantId,
      integrationId: this.integrationId,
      application: {
        name: APPLICATION_NAME, // If the app doesn't exist it will be created
        integration_id: this.integrationId
      },
      dataSet: METRIC_DATA_SET,
      record: {
        recordName: 'Vulnerabilities', // The human readable name of the record
        pluginType: 'ucv-ext-veracode', // Identifier of the plugin
        dataFormat: 'count', // Can be percent, countset, count, or entries
        metricDefinitionId: METRIC_DEFINITION_ID, // Groups metrics into Insights charts
        metricsRecordUrl: 'https://api.veracode.com/', // Link to integration server record
        metricsRecordId: 'sample-metric-1',
        entries: [ // The entry results make up the value of the metric
          {
            suite: 'Very-High', // The collection of tests (test suite)
            status: '-1'
          },
          {
            suite: 'High', // The collection of tests (test suite)
            status: '-1'
          },
          {
            suite: 'Medium',
            status: '-1'
          },
          {
            suite: 'Low',
            status: '-1'
          },
          {
            suite: 'Info',
            status: '-1'
          }
        ],
        /* executionDate: CREATION_DATE, // When the test was executed or created */
        executionDate: new Date(),
        value: {
          'Very-High': -1,
          'High': 0,
          'Medium': -1,
          'Low': -1,
          'Info': -1
        }
      }
    }
  }

  /*
  curl -k -X  POST "https://u2004da.local.net:443/api/v1/metrics" \
  -H "content-type: application/json" \
  -H "Authorization: UserAccessKey b1507795-e870-416a-ae12-06466aacc79e" \
  --data '{"tenantId": "5ade13625558f2c6688d15ce","dataSet": "DynamicScan","record":
  { "metricDefinitionName": "Application Vulnerabilities",
    "recordName": "My tutorial record",
    "pluginType": "plugin",
    "dataFormat": "custom",
    "executionDate": "2020-10-14T21:09:00.000Z",\n
    "value":
    {\n    "High": 4,\n    "Medium": 8,\n    "Low": 3,\n    "Info": 2\n  },
    \n  "description": "Tutorial data record"\n},\n"application": {\n  "name": "JPetStore"\n}\n    }'
  */
  /*
    Provides data to demonstrate the format of the IssueDataIn object that the
    uploadIssueData mutation is expecting as input (Please see the GraphQL API
    documentation mentioned in the initial comment of this file).

    Note that the data defined in the issues array should come from an external
    integration server. The rawIssueData field should contain this data unmodified,
    while the other fields are determined by normalizing the raw data to fit into
    UrbanCode Velocity issue fields.
  */
  static getSampleIssueData () {
    return {
      source: 'SAMPLE-PLUGIN', // A required identifier for data from this plugin
      trackerId: this.integrationId, // The ID of the integration (called trackerId for issues)
      tenantId: this.tenantId,
      issues: [
        {
          _id: 'sample-issue-1',
          id: 'sample-issue-1',
          name: 'Sample Issue 1',
          creator: 'sampleuser@urbancode.com',
          owner: 'sampleuser@urbancode.com',
          status: 'New',
          created: CREATION_DATE, // The date the issue was created
          lastUpdate: CREATION_DATE, // The date the issue last changed
          type: 'User Story',
          priority: '2',
          url: 'https://urbancode.com', // Link to the issue on the integration server
          description: 'The first sample issue.',
          labels: ['template-plugin', 'sample-data'],
          history: [ // The history of changed fields on the issue
            {
              timestamp: CREATION_DATE,
              content: {
                field: 'owner',
                type: 'string',
                from: '', // Old value of the field
                to: 'sampleuser@urbancode.com', // New value of the field
                user: 'sampleuser@urbancode.com', // User who made the change
                userDisplayName: 'Sample User'
              }
            }
          ],
          rawIssue: {} // The raw issue data coming from the integration server
        },
        {
          _id: 'sample-issue-2',
          id: 'sample-issue-2',
          name: 'Sample Issue 2',
          creator: 'sampleuser@urbancode.com',
          owner: 'sampleuser@urbancode.com',
          status: 'Active',
          created: CREATION_DATE, // The date the issue was created
          lastUpdate: CREATION_DATE, // The date the issue last changed
          type: 'User Story',
          priority: '2',
          url: 'https://urbancode.com', // Link to the issue on the integration server
          description: 'The second sample issue.',
          labels: ['template-plugin', 'sample-data'],
          history: [ // The history of changed fields on the issue
            {
              timestamp: CREATION_DATE,
              content: {
                field: 'owner',
                type: 'string',
                from: '', // Old value of the field
                to: 'sampleuser@urbancode.com', // New value of the field
                user: 'sampleuser@urbancode.com', // User who made the change
                userDisplayName: 'Sample User'
              }
            }
          ],
          rawIssue: {} // The raw issue data coming from the integration server
        }
      ]
    }
  }

  /*
    Provides data to demonstrate the format of the BuildDataIn object that the
    uploadBuildData mutation is expecting as input (Please see the GraphQL API
    documentation mentioned in the initial comment of this file).

    Note that the data defined in the issues array should come from an external
    integration server. The rawIssueData field should contain this data unmodified,
    while the other fields are determined by normalizing the raw data to fit into
    UrbanCode Velocity issue fields.
  */
  static getSampleBuildData () {
    return {
      id: 'sample-build-1',
      tenantId: this.tenantId,
      name: 'Sample Build 1',
      versionName: 'Sample Build 1',
      status: 'success', // Can be start, in_progress, success, failure, or unstable
      application: {
        name: APPLICATION_NAME, // If the app doesn't exist it will be created
        integration_id: this.integrationId
      },
      url: 'https://urbancode.com', // Link to the build on the integration server
      startTime: CREATION_DATE,
      endTime: CREATION_DATE,
      requestor: 'sampleuser@urbancode.com',
      revision: '123456789abcdefghi', // The SHA of the commit when the build was generated
      labels: ['template-plugin', 'sample-data'],
      source: 'SAMPLE-PLUGIN', // A required identifier for data from this plugin
      branch: 'master',
      steps: [
        {
          name: 'Checkout code',
          status: 'success',
          message: 'Code checked out successfully.',
          isFatal: false
        },
        {
          name: 'Run tests',
          status: 'success',
          message: 'Tests passed.',
          isFatal: false
        }
      ]
    }
  }
}

/*********************************************************************
* The samplePropertyPrinter script provides some functions for displaying
* the sample integration property values (Sample String Property, Sample
* Secure String Property, Sample Boolean Property, Sample Select Property,
* and Sample Dropdown Property).
*
* These sample properties are used only to gain understanding of the syntax
* for defining various properties in UrbanCode Velocity, how they are
* displayed in the UI, and how their values are resolved.
***********************************************************************/
import log4js from '@velocity/logger'
import Table from 'cli-table'

/* Acquire logger with redacted secure values. */
const LOGGER = log4js.getLogger('SamplePropertyPrinter')

export default function printSampleProperties (properties) {
  const table = new Table({
    head: ['Property', 'Value'],
    colWidths: [28, getMaxPropLength(properties) + 3]
  })
  table.options.chars.left = ' '
  table.options.chars.right = ' '
  table.options.chars.middle = ' '
  table.push(
    ['id', properties.api_id ? properties.api_id : ''],
    ['key', properties.api_key ? properties.api_key : ''],
    ['Appname in VC', properties.nameOfApp ? properties.nameOfApp : ''],
    ['Appname in Pipeline', properties.nameOfPipelineApp ? properties.nameOfPipelineApp : '']
  )

  LOGGER.info(`Sample Input Properties:\n${table.toString()}`
  )
}

function getMaxPropLength (properties) {
  const values = Object.values(properties)
  return values.reduce((val1, val2) => {
    const length1 = val1.toString().length
    const length2 = val2.toString().length
    return length1 > length2 ? val1.toString() : val2.toString()
  }).length
}
